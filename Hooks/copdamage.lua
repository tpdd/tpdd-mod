-- TODO: Test explosive headshot sync effect and damage
local Net = _G.LuaNetworking

local CopDamage_damage_bullet = CopDamage.damage_bullet
function CopDamage:damage_bullet(attack_data)
	if self._dead or self._invulnerable then
		return CopDamage_damage_bullet(self, attack_data)
	end
	
	-- mx_log("\tBeg DMG: " .. attack_data.damage)
	
	-- Enemy Armor Damage (Assault Rifle)
	self._has_plate = true
	if self._has_plate and attack_data.col_ray.body and attack_data.col_ray.body:name() == self._ids_plate_name and not attack_data.armor_piercing and attack_data.weapon_unit:base():is_category("assault_rifle") then
		self._has_plate = false
		attack_data.damage = attack_data.damage * (math.random(18, 24) * 0.01) -- Min: 0.18 Mid: .21 Max: 0.24 (6 possible values)
	end

	-- Critical Hit Chance (Submachine Gun)
	if attack_data.weapon_unit:base():is_category("smg") then
		local damage = attack_data.damage
		local critical_hits = self._char_tweak.critical_hits or {}
		local critical_value = math.random(1, 100) -- 100 possible values

		if critical_value < 22 then
			local critical_damage_mul = critical_hits.damage_mul or self._char_tweak.headshot_dmg_mul or 2 -- 2x multiplier
			attack_data.damage = critical_damage_mul and damage * critical_damage_mul
			managers.hud:on_crit_confirmed()
		end
	end
	
	-- Explosive Headshot (Sniper)
	if attack_data.weapon_unit:base():is_category("snp") and self._head_body_name and attack_data.col_ray.body and attack_data.col_ray.body:name() == self._ids_head_body_name then
		mx_log("boom")
		-- Some definitions
		local wpn_tweak_data = attack_data.weapon_unit:base():weapon_tweak_data()
		local SLOT_MASK = managers.slot:get_mask("enemies")
		local col_ray = attack_data.col_ray
		local user_unit = managers.player:player_unit()
		local tweak_damage = wpn_tweak_data.TPPD.explosive_damage	-- Defined in weapon tweak_data; explosive_damage
		local tweak_range = wpn_tweak_data.TPPD.explosive_range		-- Defined in weapon tweak_data; explosive_range
		
		-- Do explosive damage (native sync)
		local bodies = World:find_units_quick("sphere", col_ray.position, tweak_range, SLOT_MASK)
		local action_data = {
			variant = "explosion",
			damage = tweak_damage,
			weapon_unit = attack_data.weapon_unit,
			attacker_unit = user_unit,
			col_ray = col_ray
		}
		for _, hit_unit in ipairs(bodies) do
			if hit_unit:character_damage() then
				hit_unit:character_damage():damage_explosion(action_data)
			end
		end
		
		-- Spawn effects
		local EXP_DMG = 0.1
		-- managers.explosion:give_local_player_dmg(col_ray.position, range, damage) 									-- Damages player
		InstantExplosiveBulletBase:on_collision(col_ray, attack_data.weapon_unit, user_unit, EXP_DMG, false) 			-- Deals minimal damage by itself.
		local use_big_boom = wpn_tweak_data.TPPD.big_headshot
		if wpn_tweak_data and wpn_tweak_data.TPPD and use_big_boom then													-- Big explosion dictated by weapon tweak_data flag
			managers.explosion:explode_on_client(col_ray.position, col_ray.normal, user_unit, EXP_DMG, tweak_range, 1)	-- For effect only
		end
		
		-- Sync effects
		local data = {col_ray.position, col_ray.normal, user_unit, tweak_range} -- I'm skeptical about user_unit var.
		local request_id = TPPDSync.BIG_EXPLOSION_FX
		Net:SendToPeers(request_id, data)
		mx_log("Sent big exp fx sync")
	end
	
	-- mx_log("End DMG: " .. attack_data.damage)
	
	return CopDamage_damage_bullet(self, attack_data)
end