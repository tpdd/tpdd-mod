local Net = _G.LuaNetworking
TPPDSync = TPPDSync or class()
TPPDSync.HOST_CHECK_ID = "TPPDSync_host_check"				-- Client sends to host to check if host has mod.
TPPDSync.HOST_CONFIRMATION_ID = "TPPDSync_host_present"		-- Host responds to client's request.
TPPDSync.BIG_EXPLOSION_FX = "TPPDSync_big_explosion"		-- Big explosion effect.

function TPPDSync:init()
	mx_log("TPPDSync INIT")
	self._spawned_in = self._spawned_in or false
	self._is_host = self._is_host or false
	self._host_responded = self._host_responded or false
	self:setup_network()
	self:setup_sync()
	self:setup_spawn()
end

function TPPDSync:setup_network() -- Setup network handler											-- Terrible way to do syncing. Refactor later because I'm lazy.
	Hooks:Add("NetworkReceivedData", "NetworkReceivedData_TPPDSync", function(sender, id, data)		-- On incoming sync
		if id == tostring(TPPDSync.HOST_CHECK_ID) then												-- Incoming sync checks if host (player) has mod
			local request_id = tostring(TPPDSync.HOST_CONFIRMATION_ID)
			Net:SendToPeer(sender, request_id, "request")											-- Host sends a response message
			mx_log("TPPDSync_SYNC | TPPDSync_host_check received, sent TPPDSync_host_present")
			
		elseif id == tostring(TPPDSync.HOST_CONFIRMATION_ID) and Net:IsClient() then				-- Incoming sync confirms host (other player) has mod
			self._host_responded = true																-- Enable features that require modded host
			mx_log("TPPDSync_SYNC | TPPDSync_host_present received")
			
		elseif id == tostring(TPPDSync.BIG_EXPLOSION_FX) then										-- Do big explosion effect
			-- data = {col_ray.position, col_ray.normal, user_unit, tweak_range}
			local EXP_DMG = 0.1
			mx_log("TPPDSync_SYNC | Spawning explosion on client")
			managers.explosion:explode_on_client(data[1], data[2], data[3], EXP_DMG, data[4], 1)	-- data[3] may use wrong value
		end
	end)
end

function TPPDSync:setup_spawn() -- On spawn
	if PlayerBase then
		Hooks:PostHook(PlayerBase, "init", "TPPDSync_spawn_in", function(self, unit)
		
			mx_log("Spawning in; host is: "..tostring(TPPDSync._host_responded))
			mx_log("TPPDSync_prime_spawn | I spawned in.")
			TPPDSync._spawned_in = true
			
			-- Stuff that is called on spawn in.
			-- do stuff here
			
		end)
	end
end

function TPPDSync:setup_sync() -- On (any lobby) join
	if Net:IsHost() then
		mx_log("Defaulting as Host")
		self._is_host = true
		self._host_responded = true
	else
		mx_log("Checking as Client")
		Hooks:PostHook(ClientNetworkSession, "on_load_complete", "TPPDSync_lobby_join", function(self)		-- Do stuff when finished loading as client.
		
			DelayedCalls:Add("TPPDSync_ping_host", 0.5, function()											-- Wait 0.5s then check if host has mod.
				mx_log("\tSent request: "..tostring(0.5))
				if managers.network:session() and managers.network:session():server_peer():id() then		-- Only send if a server exists to recieve the request.
					local request_id = tostring(TPPDSync.HOST_CHECK_ID)
					Net:SendToPeer(managers.network:session():server_peer():id(), request_id, "request")
				else
					mx_log("\tFAILED TO SEND! NO SERVER FOUND!")
				end
			end)
		
		end)
	end
end
