-- This file is meant to run near the top of the stack.

--[[ Brief overview of nomenclature:
self._underscore_var 	means initialized at the start of the file or class.
self.ALL_CAPS 			means to be held constant value. Do not change any of these values.
self.standrd_name 		means standard value to be manipulated and passed around as usual
s	(single character)	means either iterator, local var, function argument, and/or I can't be bothered to give it a full name.
]]

_G = _G or {}
_G.TPPD = _G.TPPD or {}

-- Prints a line to the console or [crash]log.txt. Requires developer.
_G.TPPD.developer = _G.TPPD.developer or 0 -- 0: Unchecked 1: Standard 2: Developer
mx_log = mx_log or function(s)
	-- Check for the existence of the following file. File's contents do not matter.
	-- ../steamapps/common/PAYDAY 2/TPPD_DEV.txt
	if _G.TPPD.developer == 0 then
		local DEVELOPER_FILE_NAME = "TPPD_DEV.txt", file
		file = io.open(DEVELOPER_FILE_NAME, "r") -- File cannot be modified while PD2 is open if developer is enabled.
		if file ~= nil then
			_G.TPPD.developer = 2 -- Developer granted iff developer has not been previously granted and developer file exists.
			log("[TPPD] Developer Enabled")
		else
			_G.TPPD.developer = 1
		end
	end
	
	-- Output log | Skip if nondeveloper.
	if _G.TPPD.developer == 2 then
		log("[TPPD] " .. s)
		-- managers.chat:_receive_message(1, "TPPD", tostring(s), Color('00ff00'))
	end
end

Hooks:PostHook(NetworkManager, "init", "TPPDSync_init", function()
	TPPD.SYNC = TPPDSync:new()
end)

--regex function finder (function) (\w.*)(:|\.)(.*)\((.*)\)