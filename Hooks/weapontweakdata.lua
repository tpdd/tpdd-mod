local old_init = WeaponTweakData.init
function WeaponTweakData:init(tweak_data)
	old_init(self, tweak_data)
	
	-- Initialize TPPD tweak_data for every gun
	for k, weapon in pairs(self) do 
		weapon.TPPD = {}
	end
	
	-- Explosive Headshot --
	-- Big explosion
	self.r93.TPPD.big_headshot					= false 	-- R93
	self.siltstone.TPPD.big_headshot			= false 	-- GROM
	self.mosin.TPPD.big_headshot 				= false 	-- Nagant
	self.m95.TPPD.big_headshot 					= true 		-- Thanatos
	self.desertfox.TPPD.big_headshot 			= false 	-- Desertfox
	self.model70.TPPD.big_headshot 				= false 	-- Platypus
	self.msr.TPPD.big_headshot 					= false 	-- Rattlesnake
	self.wa2000.TPPD.big_headshot 				= false 	-- Lebensauger
	self.winchester1874.TPPD.big_headshot 		= false 	-- Repeater
	self.tti.TPPD.big_headshot 					= false 	-- Contractor
	
	-- Explosive Damage
	self.r93.TPPD.explosive_damage				= 100 		-- R93
	self.siltstone.TPPD.explosive_damage		= 100 		-- GROM
	self.mosin.TPPD.explosive_damage 			= 100 		-- Nagant
	self.m95.TPPD.explosive_damage 				= 300 		-- Thanatos
	self.desertfox.TPPD.explosive_damage 		= 100 		-- Desertfox
	self.model70.TPPD.explosive_damage 			= 100 		-- Platypus
	self.msr.TPPD.explosive_damage 				= 100 		-- Rattlesnake
	self.wa2000.TPPD.explosive_damage 			= 100 		-- Lebensauger
	self.winchester1874.TPPD.explosive_damage 	= 100 		-- Repeater
	self.tti.TPPD.explosive_damage 				= 100 		-- Contractor
	
	-- Explosive Range
	self.r93.TPPD.explosive_range				= 500 		-- R93
	self.siltstone.TPPD.explosive_range			= 500 		-- GROM
	self.mosin.TPPD.explosive_range 			= 500 		-- Nagant
	self.m95.TPPD.explosive_range 				= 750 		-- Thanatos
	self.desertfox.TPPD.explosive_range 		= 500 		-- Desertfox
	self.model70.TPPD.explosive_range 			= 500 		-- Platypus
	self.msr.TPPD.explosive_range 				= 500 		-- Rattlesnake
	self.wa2000.TPPD.explosive_range 			= 500 		-- Lebensauger
	self.winchester1874.TPPD.explosive_range 	= 500 		-- Repeater
	self.tti.TPPD.explosive_range 				= 500 		-- Contractor
	
end