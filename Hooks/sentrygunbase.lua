SentryGunBase = SentryGunBase or class(UnitBase)
SentryGunBase.DEPLOYEMENT_COST = {
	0.7,
	0.75,
	0.8,
	1.0 --Modded!
}
SentryGunBase.MIN_DEPLOYEMENT_COST = 0.0 --Modded!
death_time = 0

local TPDD_Turret = true

--[[ All this code need to be writen again ]]

local SentryGunBase_spawn = SentryGunBase.spawn
function SentryGunBase.spawn(owner, pos, rot, peer_id, verify_equipment, unit_idstring_index)
	if TPDD_Turret then -- IF Fire turret enabled
		local attached_data = SentryGunBase._attach(pos, rot)
		if not attached_data then
			return
		end
		if verify_equipment and not managers.player:verify_equipment(peer_id, "sentry_gun") then
			return
		end
		local sentry_owner
		if owner and owner:base().upgrade_value then
			sentry_owner = owner
		end

		local player_skill = PlayerSkill
		local ammo_multiplier = player_skill.skill_data("sentry_gun", "extra_ammo_multiplier", 1, sentry_owner)
		local armor_multiplier = 1 + (player_skill.skill_data("sentry_gun", "armor_multiplier", 1, sentry_owner) - 1) + (player_skill.skill_data("sentry_gun", "armor_multiplier2", 1, sentry_owner) - 1)
		local spread_level = player_skill.skill_data("sentry_gun", "spread_multiplier", 1, sentry_owner)
		local rot_speed_level = player_skill.skill_data("sentry_gun", "rot_speed_multiplier", 1, sentry_owner)
		local ap_bullets = player_skill.has_skill("sentry_gun", "ap_bullets", sentry_owner)
		local has_shield = player_skill.has_skill("sentry_gun", "shield", sentry_owner)
		local id_string = Idstring("units/payday2/equipment/gen_equipment_sentry/gen_equipment_sentry")
		if unit_idstring_index then
			id_string = tweak_data.equipments.sentry_id_strings[unit_idstring_index] --modded? --Idstring("units/payday2/equipment/gen_equipment_sentry/gen_equipment_sentry_silent")
		end
		local unit = World:spawn_unit(id_string, pos, rot)
		local spread_multiplier = SentryGunBase.SPREAD_MUL[spread_level]
		local rot_speed_multiplier = SentryGunBase.ROTATION_SPEED_MUL[rot_speed_level]
		managers.network:session():send_to_peers_synched("sync_equipment_setup", unit, 0, peer_id or 0)
		ammo_multiplier = SentryGunBase.AMMO_MUL[ammo_multiplier]
		unit:base():setup(owner, ammo_multiplier, armor_multiplier, spread_multiplier, rot_speed_multiplier, has_shield, attached_data)
		local owner_id = unit:base():get_owner_id()
		if ap_bullets and owner_id then
			local fire_mode_unit = World:spawn_unit(Idstring("units/payday2/equipment/gen_equipment_sentry/gen_equipment_sentry_fire_mode"), unit:position(), unit:rotation())
			unit:weapon():interaction_setup(fire_mode_unit, owner_id)
			managers.network:session():send_to_peers_synched("sync_fire_mode_interaction", unit, fire_mode_unit, owner_id)
		end
		local team
		if owner then
			team = owner:movement():team()
		else
			team = managers.groupai:state():team_data(tweak_data.levels:get_default_team_ID("player"))
		end
		unit:movement():set_team(team)
		unit:brain():set_active(true)
		SentryGunBase.deployed = (SentryGunBase.deployed or 0) + 1
		-- Lifetime & cooldown
		time = TimerManager:game():time()
		life_time = 60 * 1.50  --managers.player:upgrade_value("sentry_gun", "duration_boost", 1)
		cooldown  = 60 * 1.50 --managers.player:upgrade_value("sentry_gun", "cooldown_boost", 1) 
		death_time = time + life_time
		return unit, spread_level, rot_speed_level
	else
		return SentryGunBase_spawn(self,owner, pos, rot, peer_id, verify_equipment, unit_idstring_index)
	end
end

local SentryGunBase_setup = SentryGunBase.setup
function SentryGunBase:setup(owner, ammo_multiplier, armor_multiplier, spread_multiplier, rot_speed_multiplier, has_shield, attached_data)
	if TPDD_Turret then -- IF Fire turret enabled
		if Network:is_client() and not self._skip_authentication then
			self._validate_clbk_id = "sentry_gun_validate" .. tostring(unit:key())
			managers.enemy:add_delayed_clbk(self._validate_clbk_id, callback(self, self, "_clbk_validate"), Application:time() + 60)
		end
		self._attached_data = attached_data
		self._ammo_multiplier = ammo_multiplier
		self._armor_multiplier = armor_multiplier
		self._spread_multiplier = spread_multiplier
		self._rot_speed_multiplier = rot_speed_multiplier
		if has_shield then
			self:enable_shield()
		end
		local ammo_amount = tweak_data.upgrades.sentry_gun_base_ammo * ammo_multiplier
		self._unit:weapon():set_ammo(ammo_amount)
		local armor_amount = tweak_data.upgrades.sentry_gun_base_armor * armor_multiplier * 3000
		self._unit:character_damage():set_health(armor_amount, 0)
		self._owner = owner
		if owner then
			local peer = managers.network:session():peer_by_unit(owner)
			if peer then
				self._owner_id = peer:id()
				if self._unit:interaction() then
					self._unit:interaction():set_owner_id(self._owner_id)
				end
			end
		end
		self._unit:movement():setup(rot_speed_multiplier)
		self._unit:brain():setup(1 / rot_speed_multiplier)
		self:register()
		self._unit:movement():set_team(owner:movement():team())
		local setup_data = {
			user_unit = self._owner,
			ignore_units = {
				self._unit,
				self._owner
			},
			expend_ammo = not TPDD_Turret,
			autoaim = true,
			alert_AI = true,
			alert_filter = self._owner:movement():SO_access(),
			spread_mul = spread_multiplier,
			creates_alerts = true
		}
		self._unit:weapon():setup(setup_data)
		self._unit:set_extension_update_enabled(Idstring("base"), true)
		self:post_setup()
		return true
	else
		return SentryGunBase_setup(self,owner, ammo_multiplier, armor_multiplier, spread_multiplier, rot_speed_multiplier, has_shield, attached_data)
	end
end

local SentryGunBase_on_death = SentryGunBase.on_death
function SentryGunBase:on_death()
	if TPDD_Turret then -- IF Fire turret enabled
		if self._unit:interaction() then
			self._unit:interaction():interact()
		end
		self._unit:set_extension_update_enabled(Idstring("base"), false)
		self:unregister()
	else
		return SentryGunBase_on_death(self)
	end
end


local SentryGunBase_update = SentryGunBase.update
function SentryGunBase:update(unit, t, dt)
	if TPDD_Turret then --modded!
		if t > death_time then
			self._unit:character_damage():die() --Kill sentry on time over
		end
	end
	return SentryGunBase_update(self,unit, t, dt)
end