local InstantBulletBase_on_collision = InstantBulletBase.on_collision
local Net = _G.LuaNetworking
function InstantBulletBase:on_collision(col_ray, weapon_unit, user_unit, damage, blank, no_sound)
	if Net:IsHost() and user_unit == managers.player:player_unit() then -- Now works only as host
		for c_key, c_data in pairs(managers.groupai:state():all_player_criminals()) do
			local peer = managers.network:session():peer_by_unit(c_data.unit)
			if peer then
				if peer ~= managers.network:session():local_peer() then
					local max_distance = 400
					local distance =  mvector3.distance(col_ray.unit:position(), c_data.unit:position()) -- Calc of distance between the ray target postion and heister nearby
					if distance <= max_distance then					
						local HP_Unit = safe_spawn_unit(Idstring("units/pickups/ammo/ammo_pickup"), Vector3(1, 1, -9999), Rotation()) -- Healing Placeholder unit
						managers.network:session():send_to_peer_synched(peer, "sync_unit_event_id_16", HP_Unit, "pickup", 2 + 13) -- Healing send
                        World:delete_unit(HP_Unit) -- Delete Placeholder
                        if c_data.unit:interaction() then
                            if c_data.unit:interaction():active() then -- can my friend getTheFuckUp?
                                c_data.unit:interaction():interact(user_unit) -- Yes get the fuck up then
                            end
                        end
					end
				end
			end
		end	
	end
	return InstantBulletBase_on_collision(self, col_ray, weapon_unit, user_unit, damage, blank, no_sound) 
end


-- function mxPrint(text,var)
	-- if not var then
		-- var = ""
	-- end
	-- text = text..tostring(var)
	-- managers.chat:_receive_message(1, managers.localization:to_upper_text( "menu_system_message" ), text, tweak_data.system_chat_color)
-- end 