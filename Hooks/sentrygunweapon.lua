	local function fire_apply(unit, damage) -- I'll apply the fire effect to the poor enemy
		local col_ray = { }
		local fire_dot_data = {
			dot_damage = 50 * 1.3, --managers.player:upgrade_value("sentry_gun", "damage_boost", 1) ,
			dot_trigger_max_distance = 3000,
			dot_trigger_chance = 100,
			dot_length = 6.1,
			dot_tick_period = 0.5
		}
		col_ray.ray = Vector3(1, 0, 0)
		col_ray.position = unit:position()
		local action_data = {}
		action_data.variant = "fire" -- poison
		action_data.damage = 0.1
		action_data.attacker_unit = nil -- managers.player:player_unit() -- or nil
		action_data.col_ray = col_ray
		action_data.fire_dot_data = fire_dot_data
		unit:character_damage():damage_fire(action_data)
	end

	local function shock_apply(unit, col_ray)
		if alive(unit:body("mover_blocker")) then --Must check
		    local action_data = {}
		    action_data.damage_effect = 0.01
		    action_data.name_id = 'zeus'
		    action_data.attacker_unit = managers.player:player_unit()
		    action_data.col_ray = col_ray
		    action_data.damage = 0.01 -- Low damage.
		    action_data.variant = "taser_tased"
		    unit:character_damage():damage_melee(action_data)
		    unit:character_damage():damage_tase(action_data)
		end
	end

	local TPDD_Turret = true
	local TPDD_Turret_Type = 2 -- 0 normal 1 fire 2 shock
	-- [[ All this code need to be writen again ]]

	local tmp_rot1 = Rotation()
	local mvec_to = Vector3()
	local unitSilenced = "Idstring(@IDc71d763cd8d33588@)"
	local unitStandard = "Idstring(@IDb1f544e379409e6c@)"

	local SentryGunWeapon__fire_raycast = SentryGunWeapon._fire_raycast
	function SentryGunWeapon:_fire_raycast(from_pos, direction, shoot_player, target_unit)
		if TPDD_Turret then
			local result = {}
			local hit_unit, col_ray
			mvector3.set(mvec_to, direction)
			mvector3.multiply(mvec_to, tweak_data.weapon[self._name_id].FIRE_RANGE)
			mvector3.add(mvec_to, from_pos)
			self._from = from_pos
			self._to = mvec_to
			if not self._setup.ignore_units then
				return
			end
			if self._use_armor_piercing and false then
				local col_rays = World:raycast_all("ray", from_pos, mvec_to, "slot_mask", self._bullet_slotmask, "ignore_unit", self._setup.ignore_units)
				col_ray = col_rays[1]
				col_ray = not col_ray or not col_ray.unit:in_slot(8) or not alive(col_ray.unit:parent()) or col_rays[2] or col_ray
			else
				col_ray = World:raycast("ray", from_pos, mvec_to, "slot_mask", self._bullet_slotmask, "ignore_unit", self._setup.ignore_units)
			end
			local player_hit, player_ray_data
			if shoot_player and col_ray then --enemy sentry?
				player_hit, player_ray_data = RaycastWeaponBase.damage_player(self, col_ray, from_pos, direction)
				if player_hit then
					local damage = self:_apply_dmg_mul(self._damage, col_ray or player_ray_data, from_pos)
					InstantBulletBase:on_hit_player(col_ray or player_ray_data, self._unit, self._unit, damage)
				end
			end
			if not player_hit and col_ray then
				local damage = self:_apply_dmg_mul(self._damage, col_ray, from_pos)
				if  TPDD_Turret then 
					local damage = self:_apply_dmg_mul(0.001, col_ray, from_pos)
				end
				hit_unit = InstantBulletBase:on_collision(col_ray, self._unit, self._unit, damage)
			end
			if (not col_ray or col_ray.unit ~= target_unit) and target_unit and target_unit:character_damage() and target_unit:character_damage().build_suppression then
				target_unit:character_damage():build_suppression(self._suppression)
				if self._unit:interaction() then
					sentryOwner = self._unit:interaction()._owner_id
				end
				local peerID = managers.network:session()._local_peer._id or 1 --My peer id.
				local mySentry = sentryOwner == peerID and (tostring(self._unit:name()) == unitSilenced or tostring(self._unit:name()) == unitStandard) --Is it my sentry?
				if mySentry then
					if col_ray then
						local max_distance = 800 * 1.5 -- managers.player:upgrade_value("sentry_gun", "range_boost", 1)
						local distance =  mvector3.distance(self._unit:position(), target_unit:position()) -- Calc of distance betwern to objects [turret - enemy]
						
						if distance <= max_distance and TPDD_Turret_Type == 1 then -- col_ray.distance is fucking broken
							fire_apply(target_unit, 10)
							--sync_sentry_mark = Menufrcrst:get_data()
							--if managers.player:has_category_upgrade("sentry_gun", "mark_boost") then
							--	if managers.player:upgrade_value("sentry_gun", "mark_boost", 1) == 1 then
							--		target_unit:contour():add("mark_enemy", sync_sentry_mark, 1)
							--	else
							--		target_unit:contour():add("mark_enemy_damage_bonus", sync_sentry_mark, 1)
							--	end
							--end
						end
						if TPDD_Turret_Type == 2 then
							shock_apply(target_unit, col_ray)
						end
					end
				end
			end
			if not col_ray or col_ray.distance > 600 then
				self:_spawn_trail_effect(direction, col_ray)
			end
			result.hit_enemy = hit_unit
			if self._alert_events then
				result.rays = {col_ray}
			end
			return result
		else
			return SentryGunWeapon__fire_raycast(self,from_pos, direction, shoot_player, target_unit)
		end
	end

local SentryGunWeapon_fire = SentryGunWeapon.fire
function SentryGunWeapon:fire(blanks, expend_ammo, shoot_player, target_unit)
	if TPDD_Turret then
		self._ammo_total = 100
		self._ammo_max = 100	
		local fire_obj = self._effect_align[self._interleaving_fire]
		local from_pos = fire_obj:position()
		local direction = fire_obj:rotation():y()
		mvector3.spread(direction, tweak_data.weapon[self._name_id].SPREAD * self._spread_mul)
		World:effect_manager():spawn(self._muzzle_effect_table[self._interleaving_fire])
		self:_spawn_muzzle_effect(from_pos, direction)
		if self._use_shell_ejection_effect then
			World:effect_manager():spawn(self._shell_ejection_effect_table)
		end
		local ray_res = self:_fire_raycast(from_pos, direction, shoot_player, target_unit)
		if self._alert_events and ray_res.rays then
			RaycastWeaponBase._check_alert(self, ray_res.rays, from_pos, direction, self._unit)
		end
		self._unit:movement():give_recoil()
		self._unit:event_listener():call("on_fire")
		return ray_res
	else
		return SentryGunWeapon_fire(self,lanks, expend_ammo, shoot_player, target_unit)
	end
end

local SentryGunWeapon_sync_ammo = SentryGunWeapon.sync_ammo
function SentryGunWeapon:sync_ammo(ammo_ratio)
	if not TPDD_Turret then
		return SentryGunWeapon_sync_ammo(self,ammo_ratio)
	else
		self._ammo_ratio = 100		
		self._unit:base():set_waiting_for_refill(false)
		if self._unit:interaction() then
			self._unit:interaction():set_dirty(true)
		end
		self._unit:event_listener():call("on_sync_ammo")
	end
end

local old_init = SentryGunWeapon.init   
function SentryGunWeapon:init(unit)
	old_init(self, unit)
	if TPDD_Turret_Type == 1 then
		self._damage = 1 -- Bullet damage NERF
		self._unit:set_extension_update_enabled(Idstring("flamethrower_effect_extension"), true)
		self._flame_effect = {}
		self._flame_effect.effect = Idstring("effects/payday2/particles/explosions/flamethrower")
		self._nozzle_effect = {}
		self._nozzle_effect.effect = Idstring("effects/payday2/particles/explosions/flamethrower_nosel")
		self._pilot_light = {}
		self._pilot_light.effect = Idstring("effects/payday2/particles/explosions/flamethrower_pilot")
		self._flame_max_range = 1000
		self._single_flame_effect_duration = 1
		self._distance_to_gun_tip = 50
		self._flamethrower_effect_collection = {}
	elseif TPDD_Turret_Type == 2 then
		self._use_armor_piercing = true
		self._damage = 0.1 -- Bullet damage NERF
		self._spread_mul = 0.1
	end
end


function SentryGunWeapon:_spawn_muzzle_effect(from_pos, direction)
	if TPDD_Turret_Type == 1 then
		local from = from_pos + direction * self._distance_to_gun_tip
		local nozzle_obj = self._unit:get_object(Idstring("fire"))
		local nozzle_pos = nozzle_obj:position()
		local attach_obj = self._unit
		local effect_id = World:effect_manager():spawn({
			effect = self._flame_effect.effect,
			position = nozzle_pos,
			normal = math.UP
		})
		self._last_fire_time = managers.player:player_timer():time()
		table.insert(self._flamethrower_effect_collection, {
			id = effect_id,
			position = nozzle_pos,
			direction = direction,
			been_alive = false
		})
	end
end

local mvec1 = Vector3()
local SentryGunWeapon_update = SentryGunWeapon.update
function SentryGunWeapon:update(unit, t, dt)
	if TPDD_Turret and TPDD_Turret_Type == 1 then
		if self._blink_start_t then
			local period = 0.55
			local phase = (t - self._blink_start_t) % period / period
			if phase < 0.5 then
				self._laser_unit:base():set_on()
			else
				self._laser_unit:base():set_off()
			end
		end
		if self._flamethrower_effect_collection ~= nil then
			local flame_effect_dt = self._single_flame_effect_duration / dt
			local flame_effect_distance = self._flame_max_range / flame_effect_dt
			for _, effect_entry in pairs(self._flamethrower_effect_collection) do
				local do_continue = true
				if World:effect_manager():alive(effect_entry.id) == false then
					if effect_entry.been_alive == true then
						World:effect_manager():kill(effect_entry.id)
						table.remove(self._flamethrower_effect_collection, _)
						do_continue = false
					end
				elseif effect_entry.been_alive == false then
					effect_entry.been_alive = true
				end
				if do_continue == true then
					mvector3.set(mvec1, effect_entry.position)
					mvector3.add(effect_entry.position, effect_entry.direction * flame_effect_distance)
					local raycast = World:raycast(mvec1, effect_entry.position)
					if raycast ~= nil then
						table.remove(self._flamethrower_effect_collection, _)
					else
						World:effect_manager():move(effect_entry.id, effect_entry.position)
					end
					local effect_distance = mvector3.distance(effect_entry.position, unit:position())
					if effect_distance > self._flame_max_range then
						World:effect_manager():kill(effect_entry.id)
					end
				end
			end
		end
	else
		if TPDD_Turret_Type == 2 then
			self._damage = 0.1 -- Bullet damage NERF
			self._spread_mul = 0.1 
		end
		return SentryGunWeapon_update(self,unit, t, dt)
	end
end