# TPDD - The Payday Division
This mod requires a working version of BLT and Beardlib.  
Click here to download [Beardlib].  
Click here to download [SuperBLT].

This mod aims to overhaul the way you play PD2.
Every weapon category will have a different buff.

## Current Features
 - Enemy armor damage for ARs
 - Critical hit chance for SMGs
 - Explosive headshots for snipers 
 - WIP healing gun
 - 2 new turret 
 - Client Sync
 
## Planned Features
 - 3 more unique weapon attributes/stats
 - 3 different healing kits 
 - 3 different sticky bomb
 - More features incoming 
 - 3 New usable shields
 - New equipment: Pulse
 - New equipment: Smartcover(?)
 - New equipment: Seeker mine(?)

## Development Team
 - [SubSimple]
 - [Mx]

## Credits
 - This
 - Section
 - Is
 - Empty

[SubSimple]: <https://bitbucket.org/NPsim/>
[Mx]: <https://bitbucket.org/mxswat/>

[Beardlib]: <https://modworkshop.net/mydownloads.php?action=view_down&did=14924>
[SuperBLT]: <https://superblt.znix.xyz/>